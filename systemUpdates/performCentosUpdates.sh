#!/bin/bash

MAILDIR=/home/ctpadmin/mail

ts=`date +%Y-%m-%d_%H%M%S`

/usr/bin/yum -y update 1> ${MAILDIR}/mail-${ts}.log 2>${MAILDIR}/mail-${ts}.err
chown ctpadmin ${MAILDIR}/mail-${ts}.*
chgrp ctpadmin ${MAILDIR}/mail-${ts}.* 
chmod 700 ${MAILDIR}/mail-${ts}.log

if [ ! -s ${MAILDIR}/mail-${ts}.err ]; then
    /sbin/reboot
fi
