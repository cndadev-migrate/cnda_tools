# Script to generate MD5 sum for session 

# Where all raw dir stored
CURDIR=`pwd`
DICOMLIST=/tmp/dicomDirs.txt
MD5LIST=/tmp/md5List.txt
FLAG=/tmp/dicomBusy

dicomDir=$1

# Destination directory is required. If not, echo error message and usage output.
if [ -z "$dicomDir" ]; then
	echo "DICOM directory required."
	echo ""
        echo "generateChecksum.sh dicomDir"
        exit 1
fi

if [ -f ${FLAG} ]; then
   echo "Script already running"
   exit 0
else
   echo "Generating checksums"
   touch ${FLAG} 
fi

find ${dicomDir} -maxdepth 1 -mindepth 1 -type d -mmin +10 > ${DICOMLIST}

while read dir 
do
    dirname=`echo ${dir} | cut -d/ -f5`
    complete=`ls ${dicomDir}/${dirname} | grep ${dirname}.md5`
    ts=`date +%Y-%m-%d_%H%M%S`
    if [ -z ${complete} ]; then
        echo "${ts} Generating md5 for ${dir}"
        find ${dicomDir}/${dirname} -name '*.dcm' | xargs md5sum | cut -d' ' -f1 | sort | uniq > ${MD5LIST}
        md5sum ${MD5LIST} | cut -d' ' -f1 > ${dicomDir}/${dirname}/${dirname}.md5 
    else
        echo "${ts} Found ${dir}, but md5 already generated"
    fi 
done < ${DICOMLIST}

rm ${FLAG} 
