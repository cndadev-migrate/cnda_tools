#!/bin/bash

DCMTK_HOME=/opt/cnda_tools/dcmtk
DCMDICTPATH=${DCMTK_HOME}/share/dcmtk/dicom.dic
PATH=${DCMTK_HOME}/bin:${PATH}
export PATH DCMTK_HOME DCMDICTPATH
