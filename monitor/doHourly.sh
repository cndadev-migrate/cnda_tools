#!/bin/sh

TMPDIR=/tmp/.ctp-monitor
MONITORPATH=/opt/cnda_tools/monitor

# Run small hourly checks on the CTP
${MONITORPATH}/dicomPingMonitor.sh ${TMPDIR} 
${MONITORPATH}/runEmptyResultChecks.sh ${TMPDIR} 
