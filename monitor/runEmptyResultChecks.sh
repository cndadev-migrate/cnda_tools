#!/bin/bash

checks=$1
tmpDir=$2

TOOLPATH=/opt/cnda_tools

for f in ${checks}/*
do
   #echo $f
   command=`head -1 $f`
   checkName=`echo $f | sed 's/\.\///g'`
   #echo $checkName
   cmdOutput=`eval $command`
   #echo $cmdOutput
   #echo ""
   if [ ! -z "$cmdOutput" ]; then
      echo ${cmdOutput} > ${tmpDir}/${checkName}_alert
   else
      touch ${tmpDir}/${checkName}_clear    
   fi
done
