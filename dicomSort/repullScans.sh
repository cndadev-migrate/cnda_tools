#!/bin/bash

#USE THIS CAREFULLY -- NOT FULLY TESTED YET
# A script to send certain scans

# Inputs: 1. CNDA user id  2. subject label 3. session label 4. comma-separated list of scans to repull
# Output: DICOM for scans resent to local dicom receiver

SITE=https://cnda.wustl.edu
STORAGE_DIR=/data/ctp/storage
AETITLE=CNDA-CTP

user=$1
echo $user
subj=$2
echo $subj
sess=$3
echo $sess
scanlist=`echo $4 | sed 's/,/ /g'`
echo $scanlist

JSESSION=`curl -k -u ${user} "${SITE}/data/JSESSION"`
echo $JSESSION

# For each scan
for scan in ${scanlist}
do
# Delete scans that exist in session
echo "curl -b JSESSIONID=$JSESSION -X DELETE ${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}?allowDataDeletion=true"
curl -b JSESSIONID=$JSESSION -X DELETE "${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}?allowDataDeletion=true"

# THESE LINES ONLY USED IN CASE OF MULTI-ECHO SCANS
# delete scan001
#echo "curl -b JSESSIONID=$JSESSION -X DELETE ${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}001?allowDataDeletion=true"
#curl -b JSESSIONID=$JSESSION -X DELETE "${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}001?allowDataDeletion=true"
# delete scan002
#echo "curl -b JSESSIONID=$JSESSION -X DELETE ${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}002?allowDataDeletion=true"
#curl -b JSESSIONID=$JSESSION -X DELETE "${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}002?allowDataDeletion=true"
# delete scan003
#echo "curl -b JSESSIONID=$JSESSION -X DELETE ${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}003?allowDataDeletion=true"
#curl -b JSESSIONID=$JSESSION -X DELETE "${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}003?allowDataDeletion=true"
# delete scan004
#echo "curl -b JSESSIONID=$JSESSION -X DELETE ${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}004?allowDataDeletion=true"
#curl -b JSESSIONID=$JSESSION -X DELETE "${SITE}/REST/projects/MEDEX/subjects/$subj/experiments/$sess/scans/${scan}004?allowDataDeletion=true"
# END MULTI-ECHO SCANS SECTION

# end for
done

# find session in /data/ucsd/storage
echo "cd ${STORAGE_DIR}/$sess/*/*"
cd ${STORAGE_DIR}/$sess/*/*
echo "dicomSort.sh ST-*"
/opt/cnda_tools/dicomSort/dicomSort.sh ST-* 

# For each scan
for scan in ${scanlist}
do
   echo Sending scan ${scan}
   echo "dcmsend -aec ${AETITLE} +r +sd localhost 8104 ${scan}"
   dcmsend -aec ${AETITLE} +r +sd localhost 8104 ${scan}
   rm -rf ${scan}
# end for
done


