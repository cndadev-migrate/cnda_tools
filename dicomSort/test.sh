#!/bin/bash

# A script to send certain scans

# Inputs: 1. CNDA user id  2. subject label 3. session label 4. comma-separated list of scans to repull
# Output: DICOM for scans resent to local dicom receiver

SITE=https://cnda.wustl.edu
STORAGE_DIR=/data/ctp/storage
AETITLE=CNDA-CTP

user=$1
subj=$2
sess=$3
scanlist=`echo $4 | sed 's/,/ /g'`

JSESSION=`curl -k -u ${user} "${SITE}/data/JSESSION"`
echo $JSESSION

cd ${STORAGE_DIR}/$sess/*/*

# For each scan
for scan in ${scanlist}
do
   echo Sending scan ${scan}
   echo "dcmsend -aec ${AETITLE} +r +sd localhost 8104 ${scan}"
   dcmsend -aec ${AETITLE} +r +sd localhost 8104 ${scan}
   rm -rf ${scan}
# end for
done


