#!/bin/bash

# Usage:  dicomSort.sh dicomDirectory 

echo $1 

for h in $1/*.dcm
do
   scan=`dcmdump --search 20,11 $h | cut -d[ -f2 | cut -d] -f1`
   echo $h $scan
   if [ ! -d "$scan" ]; then
      mkdir $scan
   fi
   cp $h $scan
done 
