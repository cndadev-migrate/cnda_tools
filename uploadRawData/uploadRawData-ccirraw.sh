# Script to upload raw data to CNDA 

source /opt/dcmtk/scripts/dcmtk_setup.sh

# Where all raw dir stored
ROOTDIR=/media
CURDIR=`pwd`
RAW_DATA_FILES=${CURDIR}/rawDataFiles.txt
DIRLIST=${CURDIR}/tmpDirs.txt
TMPFILE=${CURDIR}/tmpList.txt
MUTTRCFile=/opt/MuttConfig/config.muttrc
ts=`date +%Y-%m-%d_%H%M%S`
MAILFILE=/opt/uploadRawData/mail/mailfile_${ts}.txt
MAILBODY=/opt/uploadRawData/mail/mailbody_${ts}.txt
TMPRESULTS=${CURDIR}/tmpResults.csv
SESSRESULTS=${CURDIR}/sessResults.csv
LIST=${CURDIR}/tmpSess.csv
EXPLIST=${CURDIR}/forExport.csv
DISKLIST=${CURDIR}/tmpDisks.txt

host=$1
userid=$2
emails=$3

# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
	echo "Host required."
	echo ""	
	usage
	exit 1
fi

# Userid is required. If not, echo error message and usage output.
if [ -z "$userid" ]; then
	echo "User id required."
	echo ""	
	usage
	exit 1
fi

# Emails are required. If not, echo error message and usage output.
#if [ -z "$emails" ]; then
#        echo "Emails required."
#        echo ""
#        usage
#        exit 1
#fi

# REFRESH MOUNT code to be added soon
ls /media | \
while read line
do
  echo umount /media/${line}
  umount /media/$line
  if [ -b /dev/${line} ]; then
     echo "mount /dev/$line /media/$line"
     mount /dev/$line /media/$line
  else
     echo "/dev/${line} does not exist"
  fi
done
## END REFRESH MOUNT CODE

touch /tmp/scriptStarted.txt
chown jenny /tmp/scriptStarted.txt
chgrp jenny /tmp/scriptStarted.txt

# Get session id
JSESSION=`curl -k -u $userid ""https://${host}/REST/JSESSION""`
echo JSESSION $JSESSION 
responseCount=`echo $JSESSION | wc -m`
if [ $responseCount -gt 33 ]; then
  echo "${host} log in failed. Please check your password and retry."
  exit 1
fi

./keepAlive.sh ${host} ${JSESSION} &

echo "kept going"

if [ -f $LIST ]; then
   rm $LIST
fi
if [ -f $EXPLIST ]; then
   rm $EXPLIST
fi
touch $EXPLIST

if [ -f /tmp/mail*.txt ]; then
   rm -f /tmp/mail*.txt
fi

if [ -f $SESSRESULTS ]; then
   rm $SESSRESULTS
fi
if [ -f $TMPRESULTS ]; then
   rm $TMPRESULTS
fi
if [ -f $DIRLIST ]; then
   rm $DIRLIST
fi
if [ -f $DISKLIST ]; then
   rm $DISKLIST
fi
ls -l /dev/disk/by-label | grep -v total |  sed  's/ -> \.\.\/\.\.\//,/' | rev | cut -d' ' -f1 | rev > ${DISKLIST}
if [ -f $MAILFILE ]; then
   rm $MAILFILE
fi
touch $MAILFILE

if [ -f $MAILBODY ]; then
   rm $MAILBODY
fi
echo "Status                      Disk       Project     Session         RawData Folder                                        Total Files" >> $MAILBODY


echo "searching for sessions"
find $ROOTDIR -name '*.dcm' | grep -vi Siemens | grep -vi phantom | grep -vi recycle | grep -i 'ccir-00203\|ccir-00520\|ccir-00675\|ccir-00380\|ccir-00036\|ccir-00601\|ccir-00559\|ccir-00724\|ccir-00071\|ccir-00752\|ccir-00674\|ccir-00754\|ccir-00756\|ccir-00830\|ccir-00832\|ccir-00837\|ccir-00866' | rev | cut -d/ -f2-20 | rev | uniq > $DIRLIST


if [ ! -s $DIRLIST ]; then
   echo "No drives found plugged into box on `date +'%B %d, %Y'`." > $MAILBODY
else 


#echo timepoint 1
lastSession="" 
while read folder
do
        echo $folder
        #echo timepoint 2
        if [ -f $TMPFILE ]; then
           rm $TMPFILE
        fi
        if [ -f $RAW_DATA_FILES ]; then
           rm $RAW_DATA_FILES
        fi
        echo FOLDER $folder
	cd $folder
	mediaDev=`echo "${folder}" | cut -d/ -f3`
        echo mediaDev ${mediaDev}
        devDisk=`cat ${DISKLIST} | grep ${mediaDev} | cut -d, -f1`
        echo devDisk ${devDisk}
        file=`find . -name '*.dcm' | head -1`
        echo FILE $file
	#proj=`dcmdump --search 8,1030 ${file} | cut -d[ -f2 | cut -d] -f1 | cut -d^ -f2 | cut -d' ' -f1 | sed 's/-/_/'`
	proj=`dcmdump --search 8,1030 ${file} | cut -d[ -f2 | cut -d] -f1 | cut -d^ -f2 | cut -c1-10 | sed 's/-/_/'`
        echo project $proj
        dcmSubj=`dcmdump --search 10,10 $file | cut -d[ -f2 | cut -d] -f1`
	echo subject $dcmSubj
        dcmSubj2=`echo $dcmSubj | sed 's/-/_/'`
        uid=`dcmdump --search 20,d $file | cut -d[ -f2 | cut -d] -f1`
        echo uid $uid
        dcmLabel=`dcmdump --search 10,20 $file | cut -d[ -f2 | cut -d] -f1`
	echo dcmLabel $dcmLabel
        dcmLabel2=`echo $dcmLabel | sed 's/-/_/g'`
        dcmDate=`dcmdump --search 8,20 $file | cut -d[ -f2 | cut -d] -f1`
        dcmDate=`echo ${dcmDate:0:4}-${dcmDate:4:2}-${dcmDate:6:2}`
        echo dcmDate $dcmDate
        echo "curl -b JSESSIONID=$JSESSION -k https://${host}/data/projects/${proj}/experiments?columns=UID,label,date,session_ID,xnat:subjectData/label&format=csv"
        curl -b JSESSIONID=$JSESSION -k "https://${host}/data/projects/${proj}/experiments?columns=UID,label,date,session_ID,xnat:subjectData/label&format=csv" > $SESSRESULTS
        uidFound=`cat $SESSRESULTS | grep ${uid}`
        labelDateFound1=`cat $SESSRESULTS | grep -E ${dcmLabel}.*${dcmDate}`
        subjDateFound1=`cat $SESSRESULTS | grep -E ${dcmSubj}.*${dcmDate}`
        labelDateFound2=`cat $SESSRESULTS | grep -E ${dcmLabel2}.*${dcmDate}`
        subjDateFound2=`cat $SESSRESULTS | grep -E ${dcmSubj2}.*${dcmDate}` 
        if [ ! -z "${uidFound}" ]; then
            cat $SESSRESULTS | grep ${uid} > ${LIST}
        elif [ ! -z "${labelDateFound1}" ]; then 
            cat $SESSRESULTS | grep -E ${dcmLabel}.*${dcmDate} > ${LIST}
        elif [ ! -z "${subjDateFound1}" ]; then
            cat $SESSRESULTS | grep -E ${dcmSubj}.*${dcmDate} > ${LIST}
        elif [ ! -z "${labelDateFound2}" ]; then
            cat $SESSRESULTS | grep -E ${dcmLabel2}.*${dcmDate} > ${LIST}        
        elif [ ! -z "${subjDateFound2}" ]; then
            cat $SESSRESULTS | grep -E ${dcmSubj2}.*${dcmDate} > ${LIST}
        fi
        if [ -s ${LIST} ];
        then
           cat ${LIST}
           while read sess
           do
           session=`echo $sess | cut -d, -f1`
           echo session $session  
           label=`echo $sess | cut -d, -f4`
           echo label $label 
           echo "curl -b JSESSIONID=$JSESSION -k https://${host}/data/experiments/${session}/resources/RawData/files?format=csv" 
           curl -b JSESSIONID=$JSESSION -k "https://${host}/data/experiments/${session}/resources/RawData/files?format=csv" | grep -v file_content > $RAW_DATA_FILES
           zipExists=`cat $RAW_DATA_FILES | grep .zip`
	   if [ -z "${zipExists}" ];
	   then
              echo "No zipped raw files exist for ${session}"
	      # Create the RawData folder
              if [ -z ${RAW_DATA_FILES} ]; then
                 echo "curl -b JSESSIONID=$JSESSION -k -X PUT https://${host}/data/experiments/${session}/resources/RawData"
	         curl -b JSESSIONID=$JSESSION -k -X PUT "https://${host}/data/experiments/${session}/resources/RawData"
	      fi
              find . -name '*.bf' | sed 's/\.//;s/\///' > $TMPFILE
              find . -name '*.dcm' | sed 's/\.//;s/\///' >> $TMPFILE
	      while read line
              do
                #echo timepoint 3
                echo line ${line}
                uploaded=`cat $RAW_DATA_FILES | grep ${line}`
                # if .bf file not found in the list of already uploaded files,then upload
                if [ -z $uploaded ]; then
                   echo "${proj},${label},${sess},${folder},${line}" >> ${EXPLIST}                   
                   echo "curl -b JSESSIONID=$JSESSION -k -T ./${line} https://${host}/data/experiments/${session}/resources/RawData/files/${line}?inbody=true"
	           curl -b JSESSIONID=$JSESSION -k -T ./${line} "https://${host}/data/experiments/${session}/resources/RawData/files/${line}?inbody=true"
                else
                   echo "$line already uploaded to ${proj} ${label} RawData"
                fi
	      done < $TMPFILE
              if [ -f $TMPFILE ]; then
                rm $TMPFILE
              fi
	      echo "Following files uploaded for ${proj} ${label}" >> $MAILFILE
              echo "curl -b JSESSIONID=$JSESSION -k https://${host}/data/experiments/${session}/resources/RawData/files?format=csv"
	      curl -b JSESSIONID=$JSESSION -k "https://${host}/data/experiments/${session}/resources/RawData/files?format=csv" > $TMPRESULTS
              cat $TMPRESULTS 
              cat $TMPRESULTS >> $MAILFILE
              if [ "$session" != "$lastSession" ]; then
                   numDCM=`cat $TMPRESULTS | grep .dcm | wc -l`
                   numBF=`cat $TMPRESULTS | grep .bf | wc -l`        
                   #echo "Uploaded to CNDA           ${devDisk}  ${proj} ${label} ${folder} ${numDCM} DICOM Files, ${numBF} BF Files" >> $MAILBODY
              #else
                   #echo "                                                                                       ${folder}" >> $MAILBODY
              fi
              echo " "
              lastSession=$session
	   else 
              echo "Zipped raw files already existed for ${label} ${uid}"
	      echo "No files uploaded for ${proj} ${label} ${uid}. Zipped files already existed" >> $MAILFILE
	      cat $RAW_DATA_FILES >> $MAILFILE
              #if [ "$session" != "$lastSession" ]; then
              #     echo "RawData zipped in CNDA  ${devDisk}  ${proj} ${label} ${folder}" >> $MAILBODY
              #else
              #     echo "                                                               ${folder}" >> $MAILBODY
              #fi
              lastSession=$session
	      echo " "
	   fi
           done < $LIST
           rm $LIST
           rm $SESSRESULTS
        else
           echo "No CNDA session exists for ${proj} ${dcmSubj} ${dcmLabel} ${dcmDate}"
           echo "No session exists for ${proj} ${dcmSubj} ${dcmLabel} ${dcmDate}" >> $MAILFILE
           echo "CNDA session not found   ${devDisk} ${proj} ${dcmLabel} ${folder}" >> $MAILBODY
           lastSession=""
           echo " "
        fi
	if [ -f $TMPRESULTS ]; then
           rm $TMPRESULTS
        fi
        rm $RAW_DATA_FILES
done < $DIRLIST
fi

rm -rf $DISKLIST
rm -rf $DIRLIST
cd $CURDIR

# Send an email
# mutt -s "Raw Data Report" -F ${MUTTRCFile} ${emails} < ${MAILFILE}
# rm ${MAILFILE}

# close jsession
curl -X DELETE -k -b JSESSIONID=$JSESSION  "https://${host}/REST/JSESSION"

#cp $EXPLIST /tmp/forExport.csv
#chmod 777 /tmp/forExport.csv

cp $MAILFILE /tmp
cp $MAILBODY /tmp

chown jenny /tmp/mail*_*.txt
chgrp jenny /tmp/mail*_*.txt
