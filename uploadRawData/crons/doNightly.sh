#!/bin/bash

today=`date +%d%b%Y`
LOG=logs/uploadRawData-${today}.log

cd /opt/uploadRawData
./uploadRawData-ccirraw.sh cnda.wustl.edu ${USER}:${PW} > $LOG
