# Script to upload raw data to CNDA 

source /opt/cnda_tools/dcmtk/scripts/dcmtk_setup.sh

# Where all raw dir stored
#ROOTDIR=/media
CURDIR=`pwd`
RAW_DATA_FILES=/tmp/rawDataFiles.txt
DISKLIST=/tmp/diskList.txt
FLAG=/tmp/twixBusy

destDir=$1

# Destination directory is required. If not, echo error message and usage output.
if [ -z "$destDir" ]; then
	echo "Destination directory required."
	echo ""
        echo "getRawData.sh destDir"
        exit 1
fi

if [ -f ${FLAG} ]; then
   echo "Script already running"
   exit 0
else
   touch ${FLAG} 
fi

find /media -name '*.dat' > ${RAW_DATA_FILES}
ls -l /dev/disk/by-label | grep -v total |  sed  's/ -> \.\.\/\.\.\//,/' | rev | cut -d' ' -f1 | rev > ${DISKLIST}

while read file
do
    device=`echo ${file} | cut -d/ -f3`
    diskName=`cat ${DISKLIST} | grep ${device} | cut -d, -f1`
    filename=`echo ${file} | rev | cut -d/ -f1 | rev`
    complete=`ls ${destDir} | grep ${filename}`
    ts=`date +%Y-%m-%d_%H%M%S`
    if [ -z ${complete} ]; then
        echo "${ts} Copying ${diskName} ${file}"
        cp ${file} ${destDir}
        fileNoExt=`echo ${filename} | sed 's/.dat//'`
        md5sum ${file} | cut -d' ' -f1 > ${destDir}/${fileNoExt}.md5 
    else
        echo "${ts} Found ${diskName} ${file}, but already copied"
    fi 
done < ${RAW_DATA_FILES}

rm ${FLAG} 
