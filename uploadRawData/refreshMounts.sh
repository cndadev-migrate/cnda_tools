# Script to refresh USB mounts 

FLAG=/tmp/twixBusy

if [ -f ${FLAG} ]; then
   echo "Raw data copy script running"
   exit 0
fi

# REFRESH MOUNT 
find /dev -name 'sd*1' | grep 'sdb1\|sdc1\|sdd1' | cut -d/ -f3 | \
while read line
do
  echo umount /media/${line}
  umount /media/${line}
  if [ -b /dev/${line} ]; then
     echo "mount /dev/${line} /media/${line}"
     mount /dev/${line} /media/${line}
  else
     echo "/dev/${line} does not exist"
  fi
done
## END REFRESH MOUNT CODE

